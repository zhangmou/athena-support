package com.opdar.athena.support.mapper;

import com.opdar.athena.support.entities.ConversationEntity;
import com.opdar.athena.support.entities.UserEntity;
import com.opdar.plugins.mybatis.core.IBaseMapper;

import java.util.List;

/**
 * Created by shiju on 2017/7/11.
 */
public interface ConversationMapper extends IBaseMapper<ConversationEntity> {
    List<ConversationEntity> selectByUser(ConversationEntity conversationEntity);
}
