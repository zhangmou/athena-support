package com.opdar.athena.support.entities;

import com.opdar.athena.support.mapper.ConversationMapper;
import com.opdar.athena.support.mapper.SupportUserMapper;
import com.opdar.athena.support.mapper.UserMapper;
import com.opdar.plugins.mybatis.annotations.*;

import java.sql.Timestamp;

/**
 * Created by shiju on 2017/7/14.
 */
@Namespace(ConversationMapper.class)
public class ConversationEntity {
    private String id;
    private String supportId;
    private String userId;
    @Not("2")
    private Integer stat;
    private Timestamp lastTime;
    @Sort(type = Sort.SortType.DESC)
    private Timestamp startTime;
    private Timestamp endTime;
    private String appId;
    private Integer transfer;
    private String source;
    private String utmSource;
    private String remark;
    @Sort(type = Sort.SortType.DESC)
    private Timestamp createTime;
    private Timestamp updateTime;
    @Association(mapper = SupportUserMapper.class,select = "selectOne",values = {@Value(key = "id",value = "support_id")})
    private SupportUserEntity supportUserEntity;
    @Association(mapper = UserMapper.class,select = "selectOne",values = {@Value(key = "id",value = "user_id")})
    private UserEntity userEntity;
    @Association(mapper = SupportUserMapper.class,select = "selectOne",values = {@Value(key = "id",value = "source")})
    private SupportUserEntity sourceUserEntity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SupportUserEntity getSourceUserEntity() {
        return sourceUserEntity;
    }

    public void setSourceUserEntity(SupportUserEntity sourceUserEntity) {
        this.sourceUserEntity = sourceUserEntity;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSupportId() {
        return supportId;
    }

    public void setSupportId(String supportId) {
        this.supportId = supportId;
    }

    public Timestamp getLastTime() {
        return lastTime;
    }

    public void setLastTime(Timestamp lastTime) {
        this.lastTime = lastTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public SupportUserEntity getSupportUserEntity() {
        return supportUserEntity;
    }

    public void setSupportUserEntity(SupportUserEntity supportUserEntity) {
        this.supportUserEntity = supportUserEntity;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public Integer getTransfer() {
        return transfer;
    }

    public void setTransfer(Integer transfer) {
        this.transfer = transfer;
    }

    public Integer getStat() {
        return stat;
    }

    public void setStat(Integer stat) {
        this.stat = stat;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
