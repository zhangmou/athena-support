package com.opdar.athena.support.entities;

import com.opdar.athena.support.mapper.MessageTypeMapper;
import com.opdar.plugins.mybatis.annotations.Id;
import com.opdar.plugins.mybatis.annotations.Namespace;
import com.opdar.plugins.mybatis.annotations.Sort;

import java.sql.Timestamp;

/**
 * Created by shiju on 2017/8/2.
 */
@Namespace(MessageTypeMapper.class)
public class MessageTypeEntity {
    @Id
    private String id;
    private String name;
    private Integer code;
    private String struct;
    private String view;
    private String appId;
    @Sort(type = Sort.SortType.DESC)
    private Timestamp createTime;
    private Timestamp updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStruct() {
        return struct;
    }

    public void setStruct(String struct) {
        this.struct = struct;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
