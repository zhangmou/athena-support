package com.opdar.athena.support.base;

/**
 * Created by shiju on 2017/8/8.
 */
public class Result {
    private int code;
    private String msg;
    private Object result;
    public static Result valueOf(int code, String msg, Object result){
        return new Result(code,msg,result);
    }
    public static Result valueOf(int code,String msg){
        return new Result(code,msg);
    }
    public static Result valueOf(Object result){
        return new Result(result);
    }

    public Result() {
    }

    public Result(Object result) {
        this.result = result;
    }

    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Result(int code, String msg, Object result) {
        this.code = code;
        this.msg = msg;
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
